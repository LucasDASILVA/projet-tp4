using System;
using Xunit;
using tp4Console;

namespace tp4Test
{
    public class UnitTest1
    {
        [Fact]
        public void TestCalculAireCarre()
        {
            Assert.Equal(0, Program.CalculAireCarre(0));
            Assert.Equal(4, Program.CalculAireCarre(2));
            Assert.Equal(25, Program.CalculAireCarre(5));
            Assert.Equal(12.25, Program.CalculAireCarre(3.5));
        }

        [Fact]
        public void TestCalculAireRectangle()
        {
            Assert.Equal(10, Program.CalculAireRectangle(2,5));
            Assert.Equal(0, Program.CalculAireRectangle(2,0));
            Assert.Equal(7.5, Program.CalculAireRectangle(2.5,3));
            Assert.Equal(500, Program.CalculAireRectangle(10,50));
        }

        [Fact]
        public void TestCalculAireTriangle()
        {
            Assert.Equal(2, Program.CalculAireTriangle(2,2));
            Assert.Equal(3, Program.CalculAireTriangle(3,2));
            Assert.Equal(3.75, Program.CalculAireTriangle(2.5,3));
            Assert.Equal(250, Program.CalculAireTriangle(10,50));
            Assert.Equal(0, Program.CalculAireTriangle(0,0));
            Assert.Equal(0, Program.CalculAireTriangle(3,0));
        }

        [Fact]
        public void TestDetectionMajorite()
        {
            Assert.Equal(true, Program.DetectionMajorite(1999));
            Assert.Equal(false, Program.DetectionMajorite(2004));
            Assert.Equal(true, Program.DetectionMajorite(2001));
            Assert.Equal(false, Program.DetectionMajorite(2006));
        }
    }
}
