﻿using System;

namespace tp4Console
{
    public class Program
    {
        public static double CalculAireCarre(double cote)
        {
            double resultat=0.0;
            resultat = cote*cote;
            return resultat;
        }

         public static double CalculAireRectangle(double longueur,double largeur)
        {
            double resultat=0.0;
            resultat = longueur*largeur;
            return resultat;
        }

        public static double CalculAireTriangle(double Base,double hauteur)
        {
            double resultat=0.0;
            if (Base == 0 || hauteur == 0)
            {
                return 0;
            }
            resultat = (Base*hauteur)/2;
            return resultat;
        }

        public static bool DetectionMajorite(int anneeNaissance)
        {
            int age = 0;
            int anneeActuelle;
            bool majorite = false;
            anneeActuelle = DateTime.Now.Year;
            age = anneeActuelle - anneeNaissance;
            if (age >= 18)
            {
                majorite = true;
            }
            return majorite;
            
        }
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
